"use strict";

// document.addEventListener('DOMContentLoaded', () => {

let tasks = [
    {title: 'Молоко', description: 'Необходимо купить молоко', completed: false},
    {title: 'Собака', description: 'Выгулять собаку', completed: false},
    {title: 'Угорь', description: 'Съесть угря', completed: false}
];

let newTaskName         = '';
let newTaskDescription  = '';
let editTaskName        = '';
let editTaskDescription = '';
let searchValue         = '';

//основная функция перерисовки контента
const renderTask = (tasks) => {

    const tasksContainer = document.querySelector('.main__tasks_wrapper');

    const main = document.querySelectorAll('.main__task_wrapper');
    if (main.length) {
        tasksContainer.innerHTML = '';
    }

    for (let index = 0; index < tasks.length; index++) {
        let task       = document.createElement('div');
        task.className = 'main__task_wrapper';
        task.setAttribute('DataId', index);

        if (tasks[index].completed) {
            task.style.background = "lightgreen";
        }

        task.innerHTML = `<div id="main__task_layout" class="main__task_layout" onclick="completeTask()"></div>
                            <h3 onclick="completeTask()">${tasks[index].title}</h3>
                            <p onclick="completeTask()">${tasks[index].description}</p>
                            <button id="main__task_edit" class="main__task_edit" onclick="editTask()">Edit task</button>
                            <button id="main__task_delete" class="main__task_delete" onclick="deleteTask()">Delete task</button>`;

        tasksContainer.appendChild(task);
    }
};

renderTask(tasks);

//отмечание выполненных заданий
const completeTask = () => {
    const idTask = event.target.closest(".main__task_wrapper").getAttribute('dataId');

    tasks[idTask] = {...tasks[idTask], completed: !tasks[idTask].completed};

    renderTask(tasks);

    return tasks;
};

//функция валидации данных при добавлении нового таска
const addVariableValidationNewTask = () => {
    const newTask          = event.target;
    const buttonAddNewTask = document.getElementById('main__addTask_button');

    (newTask.getAttribute('data') === 'name') ? newTaskName = newTask.value : newTaskDescription = newTask.value;
    (newTaskName && newTaskDescription) ? buttonAddNewTask.disabled = false : buttonAddNewTask.disabled = true;

    return tasks;
};

//добавление нового таска
const addTask = () => {
    tasks.push({title: newTaskName, description: newTaskDescription, completed: false});

    newTaskName                                              = newTaskDescription = '';
    document.getElementById('main__addTask_name').value      = document.getElementById('main__addTask_description').value = '';
    document.getElementById('main__addTask_button').disabled = true;

    renderTask(tasks);

    return tasks;
};

//удаление тасков
const deleteTask = () => {
    const indexTask = event.target.closest(".main__task_wrapper").getAttribute('dataId');

    tasks.splice(indexTask, 1);

    renderTask(tasks);

    return tasks;
};

//рендеринг полей редактирования элемента тасков
const editTask = () => {
    const currentTask   = event.target.closest(".main__task_wrapper");
    const idCurrentTask = event.target.closest(".main__task_wrapper").getAttribute('dataId');

    currentTask.innerHTML = `
            <div class="main__edit_wrapper">
                <textarea class="main__edit_newName" name="main__edit_newName" oninput="editTaskData()" cols="30" rows="3" placeholder="New name"></textarea>
                <textarea class="main__edit_newDescription" name="main__edit_newDescription" oninput="editTaskData()" cols="30" rows="3" placeholder="New description"></textarea>
                <button id="main__edit_save" class="main__edit_save" onclick="editTaskSave()">Сохранить</button>
            </div>
    `;

    editTaskName        = document.querySelector('.main__edit_newName').value = tasks[idCurrentTask].title;
    editTaskDescription = document.querySelector('.main__edit_newDescription').value = tasks[idCurrentTask].description;
};

//валидация введенных данных при редактировании таска
const editTaskData = () => {
    const editCurrentTask  = event.target;
    const buttonAddNewTask = document.getElementById('main__edit_save');

    (editCurrentTask.getAttribute('name') === 'main__edit_newName') ? editTaskName = editCurrentTask.value : editTaskDescription = editCurrentTask.value;

    (editTaskName && editTaskDescription) ? buttonAddNewTask.disabled = false : buttonAddNewTask.disabled = true;

    return tasks;
};

//сохранение измененного таска
const editTaskSave = () => {
    const idCurrentTask = event.target.closest(".main__task_wrapper").getAttribute('dataId');

    tasks[idCurrentTask] = {title: editTaskName, description: editTaskDescription, completed: tasks[idCurrentTask].completed};

    editTaskName = editTaskDescription = '';

    renderTask(tasks);

    return tasks;
};

//функция поиска тасков
const searchTask = () => {
    searchValue = event.target.value.toUpperCase();

    let searchTasks = [];

    let filterVariable = task => task.title.toUpperCase().includes(searchValue) || task.description.toUpperCase().includes(searchValue);

    searchTasks = tasks.filter(task => {
        return filterVariable(task);
    });

    renderTask(searchTasks);

    return searchTasks;
};

//фильтрация заданий
const filteredTasks = () => {
    const filterCheck = event.target.getAttribute('data');

    const newFilteredTasks = (filterCheck === 'Completed') ? tasks.filter(task => {
        return task.completed
    }) : tasks.filter(task => {
        return !task.completed
    });

    renderTask(newFilteredTasks);

    return newFilteredTasks;
};

//сброс всех изменений
const allReset = () => {
    document.getElementById('main__addTask_name').value = '';
    document.getElementById('main__addTask_description').value = '';
    document.querySelector('.header__search_input').value = '';

    tasks.forEach((task ) => {task.completed = false});

    renderTask(tasks);

    return tasks;
};

// });